package com.bionic.sschudakov.frontEnd;

/**
 * Created by Sergey on 05.07.14.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class FrontPageBean {

    private String frontPageBody;

    public String getFrontPageBody() {
        return frontPageBody;
    }

    public void setFrontPageBody(String includedPage) {

        int pageId = 0;

        try{
            pageId = Integer.parseInt(includedPage);
        } catch (NumberFormatException e) {
            this.frontPageBody = "welcome";
        }

        switch (pageId) {
            case 0:
                this.frontPageBody = "welcome_test";
                break;
            case 1:
                this.frontPageBody = "restaurant_menu";
                break;
            case 2:
                this.frontPageBody = "payment";
                break;
            case 3:
                this.frontPageBody = "delivery";
                break;
            case 4:
                this.frontPageBody = "table_order";
                break;
            case 5:
                this.frontPageBody = "contacts_test";
                break;
            default:
                this.frontPageBody = "welcome";
        }
    }

    public String changeBody() {
        FacesContext fc = FacesContext.getCurrentInstance();
        String includedPage = fc.getExternalContext().getRequestParameterMap().get("pageViewId");
        setFrontPageBody(includedPage);
        return null;
    }
}