package com.bionic.sschudakov.service;

import com.bionic.sschudakov.dao.FoodCategoryDao;
import com.bionic.sschudakov.entity.FoodCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


@Named
@Transactional
public class FoodCategoryService {

    @Inject
    private FoodCategoryDao foodCategoryDao;


    public void createFoodCategory(String name, String description) {
        FoodCategory foodCategory = new FoodCategory();
        foodCategory.setName(name);
        foodCategory.setDescription(description);
        foodCategoryDao.saveFoodCategory(foodCategory);
    }

    public void updateFoodCategory(FoodCategory foodCategory) {
        foodCategoryDao.updateFoodCategory(foodCategory);
    }

    public FoodCategory findById(Long id) {
        return foodCategoryDao.findById(id);
    }

    public FoodCategory findFoodCategory(String name) {
        return foodCategoryDao.findByName(name);
    }

    public List<FoodCategory> getAll() {
        return foodCategoryDao.findAll();
    }

    public List<FoodCategory> getAllOrdered() {
        return foodCategoryDao.findAllOrdered();
    }

    public boolean delete(FoodCategory foodCategory) {
        return foodCategoryDao.delete(foodCategory);
    }

}
