package com.bionic.sschudakov.service;

import com.bionic.sschudakov.dao.DishPriceDao;
import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;


@Named
@Transactional
public class DishPriceService {

    @Inject
    DishPriceDao dishPriceDao;

    public void createDishPrice(DishPrice dishPrice) {
        dishPriceDao.saveDishPrice(dishPrice);
    }

    public DishPrice getById(Long id) {
        return dishPriceDao.findById(id);
    }

    public List<DishPrice> getAll() {
        return dishPriceDao.findAll();
    }

    public DishPrice getLastActualDishPrice(Dish dish, Date date) {
        return dishPriceDao.findLastActualDishPrice(dish, date);
    }

}
