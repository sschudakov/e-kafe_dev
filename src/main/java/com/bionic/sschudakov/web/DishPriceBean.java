package com.bionic.sschudakov.web;

import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import com.bionic.sschudakov.service.DishPriceService;
import com.bionic.sschudakov.service.DishService;
import com.bionic.sschudakov.service.FoodCategoryService;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Named
@Scope("request")
public class DishPriceBean {

    @Inject
    private DishPriceService dishPriceService;

    @Inject
    FoodCategoryService foodCategoryService;

//    public List<Dish> getDishesOrderedByFoodCategoryIndex() {
//        return dishService.getDishesOrderedByFoodCategoryIndex();
//    }

    public BigDecimal getLastActualDishPriceValue(Dish dish) {
        DishPrice dishPrice = dishPriceService.getLastActualDishPrice(dish, new Date());
        if (dishPrice != null) {
            Integer price = dishPrice.getPrice();
            return new BigDecimal(price).divide(new BigDecimal(100));
        }
        return BigDecimal.ZERO;
    }

    public Date getLastActualDishPriceDate(Dish dish) {
        DishPrice dishPrice = dishPriceService.getLastActualDishPrice(dish, new Date());
        if (dishPrice != null) {
            return new Date(dishPrice.getDateTime().getTime());
        }
        return null;
    }

}
