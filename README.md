# README #

Short overview of the Application

### What is this repository for? ###

* This is E-Restaurant Web Application Repository
* Version 1.0

### Used technologies ###
* JavaEE 7.0
* Spring 3
* JSF
* OpenFaces
* Tomcat 8.0
* JPA
* Hibernate
* MySQL Database
* Maven