package com.bionic.sschudakov.service;

import com.bionic.sschudakov.dao.DishDao;
import com.bionic.sschudakov.dao.FoodCategoryDao;
import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.FoodCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


@Named
@Transactional
public class DishService {

    @Inject
    private DishDao dishDao;

    @Inject
    private FoodCategoryDao foodCategoryDao;

    public void createDish(FoodCategory foodCategory, String name, String description, boolean kitchenMade) {
        Dish dish = new Dish();
        dish.setFoodCategory(foodCategory);
        dish.setName(name);
        dish.setDescription(description);
        dish.setKitchenMade(kitchenMade);
        dishDao.saveDish(dish);
    }

    public Dish getById(Long id) {
        return dishDao.findById(id);
    }

    public Dish getByName(String name) {
        return dishDao.findByName(name);
    }

    public List<Dish> getAll() {
        return dishDao.findAll();
    }

    public List<Dish> findByCategoryId(Long foodCategoryId) {
        FoodCategory foodCategory = foodCategoryDao.findById(foodCategoryId);
        return dishDao.findByCategory(foodCategory);
    }

    public boolean delete(Long dishId) {
        return dishDao.remove(dishId);
    }

    public Dish update(Dish dish) {
        return dishDao.updateDish(dish);
    }

    public List<Dish> getDishesOrderedByFoodCategoryIndex() {
        return dishDao.findAllOrderedByFoodCategoryIndex();
    }

}
