package com.bionic.sschudakov.web;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Sergey on 15.07.14.
 */
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        LoginBean loginBean = (LoginBean) req.getSession().getAttribute("loginBean");
        String url = req.getRequestURI();

        if (loginBean == null || !loginBean.isLogged) {
            // user is not logged in
            if (url.indexOf("admin_home.xhtml") >= 0 || url.indexOf("logout.xhtml") >= 0
                    || url.indexOf("edit_dishes.xhtml") >= 0 || url.indexOf("reports.xhtml") >= 0
                    || url.indexOf("set_dish_prices.xhtml") >= 0) {
                // try to access administrator resources or logout
                // redirect to login.xhtml
                resp.sendRedirect(req.getContextPath() + "login.xhtml");
            } else {
                // any other pages
                chain.doFilter(request, response);
            }
        } else {
            // user is logged in
            if (url.indexOf("login.xhtml") >= 0) {
                resp.sendRedirect(req.getContextPath() + "admin_home.xhtml");
            }
            else if (url.indexOf("logout.xhtml") >= 0){
                // user wants to logout
                req.getSession().removeAttribute("loginBean");
                resp.sendRedirect(req.getContextPath() + "login.xhtml");
            } else {
                // any other pages
                chain.doFilter(request, response);
            }
        }
    }

    @Override
    public void destroy() {

    }
}
