-- MySQL dump 10.13  Distrib 5.6.14, for Win64 (x86_64)
--
-- Host: localhost    Database: e-kafe_dev
-- ------------------------------------------------------
-- Server version	5.6.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dish`
--

DROP TABLE IF EXISTS `dish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `food_category_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `kitchen_made` tinyint(1) NOT NULL DEFAULT '0',
  `img_path` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `food_category_id_idx` (`food_category_id`),
  CONSTRAINT `food_category_id` FOREIGN KEY (`food_category_id`) REFERENCES `food_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish`
--

LOCK TABLES `dish` WRITE;
/*!40000 ALTER TABLE `dish` DISABLE KEYS */;
INSERT INTO `dish` VALUES (1,1,'Greek salad','Very tasty greek salad',1,'greek_salad.png',0),(2,4,'Ballantines','Ballantines 18 y.o.2',0,'ballantines.png',0),(3,3,'Ukrainian borsch','Traditional ukrainian borsch',1,'borsch.png',0),(4,1,'Chicken pineapple salad','Salad with chicken, pineapple and cheese',1,'chicken_pineapple_salad.png',0),(5,3,'Chicken broth','Tasty chicken broth with beans',1,'chicken-noodle.png',0),(6,4,'Mohito123','Mohito cocktail with fresh mint',1,'mohito.png',0),(7,2,'Napoleon cake','True Napoleon cake',1,'napoleon_cake.png',0),(8,3,'Pork soup','Soup with fresh vegetables and pork',1,'pork_soup.png',0),(9,2,'Cheese cake','Tasty NY cheese cake',1,'rasberry_cheesecake.png',0),(10,4,'Martini','Imported Martini',0,'martini.png',0),(11,1,'Salad with crabsticks','Salad with crab sticks, corn and rice',1,'salad-crabcake.png',0),(12,2,'Ice-cream','Vanilla ice-cream made by ukr. GOST',1,'ice-cream.png',0),(18,5,'Rostbiff','Our Cheef\'s signature dish',1,'rostbiff.png',0),(19,5,'Italian Pizza','Large pizza with pazmezan, chicken and pineapple',1,'pizza.png',0),(20,5,'Chicken Dish','Fried chicken with fresh vegetables',1,'chickenDish.png',0);
/*!40000 ALTER TABLE `dish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_price`
--

DROP TABLE IF EXISTS `dish_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_price` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dish_id` int(11) unsigned NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `price` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `dish_id_idx` (`dish_id`),
  CONSTRAINT `dish_id` FOREIGN KEY (`dish_id`) REFERENCES `dish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_price`
--

LOCK TABLES `dish_price` WRITE;
/*!40000 ALTER TABLE `dish_price` DISABLE KEYS */;
INSERT INTO `dish_price` VALUES (1,12,'2014-01-31 22:00:00',700,0),(2,3,'2014-07-09 19:56:25',1575,0),(3,1,'2014-01-31 22:00:00',1000,0),(4,8,'2014-01-31 22:00:00',1200,0),(5,2,'2014-01-31 22:00:00',3000,0),(6,8,'2014-07-17 21:00:00',1700,0),(7,2,'2014-07-17 21:00:00',3800,0),(8,3,'2014-07-17 21:00:00',2000,0),(9,1,'2014-07-17 21:00:00',1400,0),(10,12,'2014-07-17 21:00:00',900,0),(11,8,'2014-07-18 21:00:00',2200,0),(12,3,'2014-07-18 21:00:00',2500,0),(13,1,'2014-07-18 21:00:00',1900,0),(14,2,'2014-07-18 21:00:00',5500,0),(15,12,'2014-07-18 21:00:00',1200,0),(16,4,'2013-12-31 22:00:00',2475,0),(17,5,'2013-12-31 22:00:00',1250,0),(18,6,'2013-12-31 22:00:00',4500,0),(19,7,'2013-12-31 22:00:00',5020,0),(20,8,'2013-12-31 22:00:00',1516,0),(21,9,'2013-12-31 22:00:00',3220,0),(22,10,'2013-12-31 22:00:00',2500,0),(23,11,'2013-12-31 22:00:00',4780,0),(24,18,'2013-12-31 22:00:00',5600,0),(25,19,'2013-12-31 22:00:00',3850,0),(26,20,'2013-12-31 22:00:00',4500,0);
/*!40000 ALTER TABLE `dish_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_category`
--

DROP TABLE IF EXISTS `food_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT '0',
  `order_index` smallint(6) NOT NULL DEFAULT '9999',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_category`
--

LOCK TABLES `food_category` WRITE;
/*!40000 ALTER TABLE `food_category` DISABLE KEYS */;
INSERT INTO `food_category` VALUES (1,'Salads','Fresh salads',0,10,0),(2,'Desserts','Bakery, ice-cream and cakes',0,40,0),(3,'Soups','Best soups from different countries of the world',0,20,0),(4,'Beverages','Fresh juices, alcohol drinks and cocktails',0,50,0),(5,'Main Course','Pork, beaf and chicken dishes',0,30,0);
/*!40000 ALTER TABLE `food_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_order_id` int(11) unsigned NOT NULL,
  `dish_item_id` int(11) unsigned NOT NULL,
  `dish_price` int(10) unsigned NOT NULL DEFAULT '0',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `restaurant_order_id_idx` (`restaurant_order_id`),
  KEY `dish_item_id_idx` (`dish_item_id`),
  CONSTRAINT `dish_item_id` FOREIGN KEY (`dish_item_id`) REFERENCES `dish` (`id`),
  CONSTRAINT `restaurant_order_id` FOREIGN KEY (`restaurant_order_id`) REFERENCES `restaurant_order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (1,1,1,100,2,0),(2,1,2,250,3,0),(3,1,3,150,2,0),(4,2,8,50,5,0),(5,2,12,75,5,0),(6,6,1,1000,2,0),(7,6,4,2475,2,0),(8,6,11,4780,1,0),(9,8,9,3220,1,0),(10,8,7,5020,1,0),(11,8,4,2475,1,0),(12,9,5,1250,5,0),(13,10,1,1000,3,0),(14,10,11,4780,1,0),(15,10,6,4500,2,0),(16,10,12,700,1,0),(17,10,3,1575,1,0),(18,10,8,1200,1,0),(19,10,5,1250,1,0),(20,11,7,5020,4,0),(21,11,12,700,2,0),(22,11,2,3000,1,0),(23,11,6,4500,6,0),(24,12,6,4500,2,0),(25,12,10,2500,2,0),(26,13,4,2475,6,0),(27,13,2,3000,3,0),(28,13,12,700,3,0),(29,14,9,3220,1,0),(30,14,7,5020,1,0),(31,15,9,3220,3,0),(32,16,4,2475,10,0),(33,16,6,4500,10,0),(34,17,2,3000,1,0),(35,18,6,4500,1,0),(36,18,5,1250,1,0),(37,18,8,1200,1,0),(38,19,5,1250,1,0),(39,19,8,1200,1,0),(40,19,9,3220,1,0),(41,19,7,5020,1,0),(42,20,9,3220,1,0),(43,21,1,1000,4,0),(44,21,4,2475,3,0),(45,22,1,1000,5,0),(46,22,4,2475,3,0),(47,22,11,4780,4,0),(48,23,2,3000,1,0),(49,23,6,4500,5,0),(50,24,1,1000,5,0),(51,24,4,2475,9,0),(52,25,1,1000,3,0),(53,25,4,2475,5,0),(54,25,11,4780,3,0),(55,26,2,3000,7,0),(56,27,9,3220,1,0),(57,27,7,5020,1,0),(58,28,1,1000,1,0),(59,29,3,1575,1,0),(60,29,5,1250,1,0),(61,29,8,1200,1,0),(62,30,7,5020,1,0),(63,30,9,3220,1,0),(64,31,5,1250,1,0),(65,32,3,1575,2,0),(66,33,9,3220,2,0),(67,34,2,3000,2,0),(68,34,5,1250,1,0),(69,34,3,1575,2,0),(70,34,8,1200,1,0),(71,34,1,1000,1,0),(72,34,4,2475,1,0),(73,34,11,4780,1,0),(74,35,1,1000,1,0),(75,36,4,2475,3,0),(76,37,9,3220,3,0),(77,38,2,3000,3,0),(78,38,19,3850,2,0),(79,38,18,5600,5,0),(80,39,18,5600,3,0),(81,39,19,3850,3,0),(82,39,20,4500,3,0);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_order`
--

DROP TABLE IF EXISTS `restaurant_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_time_open` timestamp NULL DEFAULT NULL,
  `date_time_submit` timestamp NULL DEFAULT NULL,
  `date_time_close` timestamp NULL DEFAULT NULL,
  `comments` varchar(1024) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_order`
--

LOCK TABLES `restaurant_order` WRITE;
/*!40000 ALTER TABLE `restaurant_order` DISABLE KEYS */;
INSERT INTO `restaurant_order` VALUES (1,'2014-05-01 14:05:20','2014-06-20 14:56:20',NULL,'Table 1',0),(2,'2014-06-19 12:45:37','2014-06-20 14:56:20',NULL,'Table 3',0),(3,'2014-07-19 13:15:10','2014-06-20 14:56:20',NULL,'Table 2',0),(4,'2014-07-13 23:07:14','2014-07-13 23:09:49',NULL,'Table 4',0),(5,'2014-07-14 07:16:23','2014-07-14 07:17:58',NULL,'Table 6',0),(6,'2014-07-14 07:16:23','2014-07-14 07:21:42',NULL,'Table 3',0),(8,'2014-07-14 12:08:19','2014-07-14 12:11:53',NULL,'Table 1',0),(9,'2014-07-14 12:08:19','2014-07-14 12:26:12',NULL,'Table 8',0),(10,'2014-07-14 13:14:17','2014-07-14 13:15:22',NULL,'Table 4',0),(11,'2014-07-14 16:08:42','2014-07-14 16:10:09',NULL,'Table 3',0),(12,'2014-07-14 16:08:42','2014-07-14 16:13:17',NULL,'Table 7',0),(13,'2014-07-14 16:34:16','2014-07-14 16:34:52',NULL,'Table 5',0),(14,'2014-07-14 16:34:16','2014-07-14 16:39:41',NULL,'Table 4',0),(15,'2014-07-14 16:34:16','2014-07-14 16:55:20',NULL,'Table 2',0),(16,'2014-07-14 17:30:56','2014-07-14 17:32:40',NULL,'Table 8',0),(17,'2014-07-14 17:30:56','2014-07-14 17:38:04',NULL,'Table 1',0),(18,'2014-07-14 17:30:56','2014-07-14 17:40:07',NULL,'Table 4',0),(19,'2014-07-14 17:30:56','2014-07-14 17:41:18',NULL,'Table 2',0),(20,'2014-07-14 17:30:56','2014-07-14 17:42:15',NULL,'Table 6',0),(21,'2014-07-14 17:30:56','2014-07-14 17:48:55',NULL,'Table 5',0),(22,'2014-07-15 20:27:40','2014-07-15 20:28:20',NULL,'Table 1',0),(23,'2014-07-16 06:45:32','2014-07-16 06:54:18',NULL,'Table 3',0),(24,'2014-07-16 07:14:00','2014-07-16 07:18:32',NULL,'Table 1',0),(25,'2014-07-16 07:42:18','2014-07-16 07:42:47',NULL,'Table 6',0),(26,'2014-07-16 08:38:49','2014-07-16 08:39:32',NULL,'Table 7',0),(27,'2014-07-16 08:43:15','2014-07-16 08:43:30',NULL,'Table 8',0),(28,'2014-07-16 08:46:41','2014-07-16 08:47:04',NULL,'Table 4',0),(29,'2014-07-16 08:49:15','2014-07-16 08:49:46',NULL,'Table 3',0),(30,'2014-07-16 11:55:06','2014-07-16 11:55:18',NULL,'Table 7',0),(31,'2014-07-16 11:57:08','2014-07-16 11:57:20',NULL,'Table 3',0),(32,'2014-07-16 13:11:52','2014-07-16 13:15:42',NULL,'Table 2',0),(33,'2014-07-16 13:11:52','2014-07-16 13:15:54',NULL,'Table 2',0),(34,'2014-07-16 16:02:32','2014-07-16 16:06:54',NULL,'Table 5',0),(35,'2014-07-16 16:18:19','2014-07-16 16:21:12',NULL,'+380679876543',0),(36,'2014-07-16 19:19:50','2014-07-16 19:20:06',NULL,'+380633451287',0),(37,'2014-07-16 19:51:42','2014-07-16 19:52:06',NULL,'2345678',0),(38,'2014-07-16 22:25:21','2014-07-16 22:28:18',NULL,'12345',0),(39,'2014-08-04 14:10:51','2014-08-04 14:11:30',NULL,'1345',0);
/*!40000 ALTER TABLE `restaurant_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `user_name` varchar(32) NOT NULL,
  `password` varchar(48) NOT NULL,
  `user_role_id` int(11) unsigned DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_pyu153aa0dw1iveq002987wj7` (`user_role_id`),
  CONSTRAINT `user_role_id` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,'serg','12345',NULL,NULL,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-04 14:15:02
