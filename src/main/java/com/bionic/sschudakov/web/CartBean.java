package com.bionic.sschudakov.web;

import com.bionic.sschudakov.dao.DishPriceDao;
import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import com.bionic.sschudakov.entity.OrderItem;
import com.bionic.sschudakov.entity.RestaurantOrder;
import com.bionic.sschudakov.service.OrderItemService;
import com.bionic.sschudakov.service.RestaurantOrderService;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Sergey on 09.07.14.
 */
@Named
@Scope("session")
public class CartBean {

    @Inject
    private DishPriceDao dishPriceDao;

    private Date orderOpenDate;
    private List<CartItem> cartItems = new ArrayList<CartItem>();

    private String clientsPhoneNumber = "";

    public CartBean() {
        orderOpenDate = new Date();
    }

    public Date getOrderOpenDate() {
        return orderOpenDate;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }


    public String getClientsPhoneNumber() {
        return clientsPhoneNumber;
    }

    public void setClientsPhoneNumber(String clientsPhoneNumber) {
        this.clientsPhoneNumber = clientsPhoneNumber;
    }

    public String add(Dish dish) {

        boolean newDish = true;

        for (CartItem c : cartItems) {

            if (c.getDish().getId() == dish.getId()) {
                c.setQuantity(c.getQuantity() + 1);
                newDish = false;
                break;
            }
        }

        if (newDish) {
            CartItem cartItem = new CartItem();
            cartItem.setDish(dish);
            DishPrice dishPrice = dishPriceDao.findLastActualDishPrice(dish, orderOpenDate);
            Integer price = 0;
            if (dishPrice != null) {
                price = dishPrice.getPrice();
            }
            cartItem.setPrice(price);
            cartItem.setQuantity(new Integer(1));
            cartItems.add(cartItem);
        }

        return "";

    }

    public String remove(Dish dish) {
        for (Iterator<CartItem> it = cartItems.iterator(); it.hasNext(); ) {
            CartItem cartItem = it.next();
            if (cartItem.getDish().getId() == dish.getId()) {
                cartItem.setQuantity(cartItem.getQuantity() - 1);
                if (cartItem.getQuantity().equals(new Integer(0))) {
                    it.remove();
                }
            }
        }

        return "";
    }

    public int getCartCount() {

        int cartSize = 0;

        for (CartItem c : cartItems) {
            cartSize += c.getQuantity();
        }

        return cartSize;

    }

    public BigDecimal getCartTotalPrice() {

        Long cartPrice = 0L;
        for (CartItem c : cartItems) {
            cartPrice += (c.getQuantity() * c.getPrice());
        }

        return new BigDecimal(cartPrice).divide(new BigDecimal(100));
    }

    public String cleanCart() {
        this.cartItems = new ArrayList<CartItem>();
        return "menu";
    }

    /**
     * Method to check whether dish item has been already put to the cart.
     *
     * @param dishId
     * @return
     */
    public boolean cartHasItem(Long dishId) {

        for (CartItem c : cartItems) {
            if (c.getDish().getId().equals(dishId)) {
                return true;
            }
        }

        return false;
    }

    public int getCartItemCount(Long id) {

        int cartItemCount = 0;

        for (CartItem c : cartItems) {
            if (c.getDish().getId().equals(id))
                cartItemCount += c.getQuantity();
        }

        return cartItemCount;

    }

}
