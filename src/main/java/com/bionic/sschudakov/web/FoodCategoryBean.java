package com.bionic.sschudakov.web;

import com.bionic.sschudakov.entity.FoodCategory;
import com.bionic.sschudakov.service.FoodCategoryService;
import org.openfaces.event.SelectionChangeEvent;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@Scope("request")
public class FoodCategoryBean {

    private static int sCount = 0;

    @Inject
    private FoodCategoryService foodCategoryService;

    private String name;
    private String description;

    private List<FoodCategory> foodCategoriesList;

    private Long selectedFoodCategoryId = 0L;

    public FoodCategoryBean() {
        sCount++;
        System.out.println("FoodCategoryBean was created: " + sCount + " times");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FoodCategory> getFoodCategories() {
        return foodCategoryService.getAll();
    }

    public List<FoodCategory> getFoodCategoriesOrdered() {
        return foodCategoryService.getAllOrdered();
    }

    public void createFoodCategory() {
        foodCategoryService.createFoodCategory(name, description);
    }

    public void updateFoodCategory(FoodCategory foodCategory) {
        foodCategoryService.updateFoodCategory(foodCategory);
    }

    public FoodCategory getById(Long id) {
        return foodCategoryService.findById(id);
    }

    public void selectedFoodCategoryChanged(SelectionChangeEvent e) {
        System.out.println("" + e.getOldIndex() + " -> " + e.getNewIndex());
        int a = 0;

    }

    public void changeFoodCategoryIndex(Long foodCategoryId) {
        this.selectedFoodCategoryId = foodCategoryId;
    }

    public FoodCategory getSelectedFoodCategory() {
        return foodCategoryService.findById(selectedFoodCategoryId);
    }

}
