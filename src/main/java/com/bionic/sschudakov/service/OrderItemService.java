package com.bionic.sschudakov.service;

import com.bionic.sschudakov.dao.OrderItemDao;
import com.bionic.sschudakov.entity.OrderItem;
import com.bionic.sschudakov.web.CartBean;
import org.springframework.transaction.annotation.Transactional;

import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


@Named
@Transactional
public class OrderItemService {

    @Inject
    private OrderItemDao orderItemDao;

    public void createOrderItem(OrderItem orderItem) {
        orderItemDao.saveOrderItem(orderItem);
    }

    public List<OrderItem> getAllOrderItems() {
        return orderItemDao.findAll();
    }


    public OrderItem getOrderItemById(Long id) {
        return orderItemDao.findById(id);
    }

}
