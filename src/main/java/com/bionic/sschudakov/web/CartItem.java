package com.bionic.sschudakov.web;

import com.bionic.sschudakov.entity.Dish;

import java.math.BigDecimal;

/**
 * Created by Sergey on 10.07.14.
 */
public class CartItem {

    private Dish dish;
    private Integer price;
    private Integer quantity;

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public BigDecimal getDecimalPrice() {
        return new BigDecimal(price).divide(new BigDecimal(100));
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
