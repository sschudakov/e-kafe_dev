package com.bionic.sschudakov.dao;

import com.bionic.sschudakov.entity.RestaurantOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class RestaurantOrderDao {
    @PersistenceContext
    private EntityManager entityManager;

    public RestaurantOrder saveRestaurantOrder(RestaurantOrder restaurantOrder) {
        entityManager.persist(restaurantOrder);
        return restaurantOrder;
    }

    public RestaurantOrder updateRestaurantOrder(RestaurantOrder restaurantOrder) {
        entityManager.merge(restaurantOrder);
        return restaurantOrder;
    }

    public RestaurantOrder findById(Long id) {
        return entityManager.find(RestaurantOrder.class, id);
    }

    public List<RestaurantOrder> findAll() {
        return entityManager.createQuery("FROM RestaurantOrder")
                .getResultList();
    }

    public boolean delete(Long id) {
        entityManager.remove(entityManager.getReference(RestaurantOrder.class, id));
        return true;
    }

}
