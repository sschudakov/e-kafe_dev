package com.bionic.sschudakov.frontEnd;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Created by Sergey on 05.07.14.
 */
@ManagedBean
@RequestScoped
public class Msgs {

    private final String frontpageLink = "Welcome";
    private final String restaurantMenuLink = "Restaurant Menu";
    private final String paymentLink = "Payment";
    private final String deliveryLink = "Delivery";
    private final String tableOrderLink = "Table order";
    private final String contactLink = "Contacts";

    public String getFrontpageLink() {
        return frontpageLink;
    }

    public String getRestaurantMenuLink() {
        return restaurantMenuLink;
    }

    public String getPaymentLink() {
        return paymentLink;
    }

    public String getDeliveryLink() {
        return deliveryLink;
    }

    public String getTableOrderLink() {
        return tableOrderLink;
    }

    public String getContactLink() {
        return contactLink;
    }
}
