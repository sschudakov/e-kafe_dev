package com.bionic.sschudakov.dao;

import com.bionic.sschudakov.entity.OrderItem;
import com.bionic.sschudakov.entity.RestaurantOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class OrderItemDao {
    @PersistenceContext
    private EntityManager entityManager;

    public OrderItem saveOrderItem(OrderItem orderItem) {
        entityManager.persist(orderItem);
        return orderItem;
    }

    public OrderItem updateOrderItem(OrderItem orderItem) {
        entityManager.merge(orderItem);
        return orderItem;
    }

    public OrderItem findById(Long id) {
        return entityManager.find(OrderItem.class, id);
    }

    public List<OrderItem> findAll() {
        return entityManager.createQuery("FROM OrderItem")
                .getResultList();
    }

    public List<OrderItem> findByRestaurantOrder(RestaurantOrder restaurantOrder) {
        return entityManager.createQuery("SELECT o from OrderItem o WHERE o.restaurantOrder = :restaurantOrder")
                .setParameter("restaurantOrder", restaurantOrder)
                .getResultList();
    }

    public boolean delete(OrderItem orderItem) {
        entityManager.remove(entityManager.getReference(OrderItem.class, orderItem.getId()));
        return true;
    }

}
