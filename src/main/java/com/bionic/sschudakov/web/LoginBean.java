package com.bionic.sschudakov.web;

import com.bionic.sschudakov.dao.DishPriceDao;
import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import com.bionic.sschudakov.entity.User;
import com.bionic.sschudakov.service.UserService;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Sergey on 09.07.14.
 */
@Named
@Scope("session")
public class LoginBean {

    @Inject
    UserService userService;

    private String userName;
    private String password;

    public boolean isLogged = false;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String processLogin() {
        User user = userService.findUser(userName);

        if (user == null || !user.getPassword().equals(password)) {
            return "bad_credentials";
        }
        isLogged = true;
        return "admin_home";
    }


}
