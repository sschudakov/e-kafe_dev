package com.bionic.sschudakov.commons.dao;

import com.bionic.sschudakov.commons.domain.BaseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Provides generic common implementation of GenericDao interface persistence methods.
 * Extend this abstract class to implement DAO for your specific needs.
 *
 * @author Sergey Chudakov
 * Created by Sergey on 26.06.14.
 */
@Repository
public abstract class GenericDaoImpl<T extends BaseEntity> implements IGenericDao<T> {

    private Class<T> persistentClass;

    @PersistenceContext
    protected EntityManager entityManager;

    protected GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        persistentClass = (Class) pt.getActualTypeArguments()[0];
    }

    protected GenericDaoImpl(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    @Override
    @Transactional(readOnly = true)
    public T findById(Long id) {
        T entity = (T) entityManager.find(persistentClass, id);
        return entity;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<T> findAll() {
        return entityManager.createQuery("FROM " + persistentClass.getSimpleName()).getResultList();
    }

    @Override
    public T save(T entity) {
            entityManager.persist(entity);
            return entity;
    }

    @Override
    public T update(T entity) {
        T mergedEntity = entityManager.merge(entity);
        return mergedEntity;
    }

    @Override
    public void delete(T entity) {
        entityManager.remove(entityManager.getReference(entity.getClass(), ((BaseEntity) entity).getId()));
    }

    @Override
    public void delete(Long id) {
        entityManager.remove(entityManager.getReference(persistentClass, id));
    }

    @Override
    public void flush() {
        entityManager.flush();
    }
}
