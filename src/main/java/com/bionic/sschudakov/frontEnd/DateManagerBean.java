package com.bionic.sschudakov.frontEnd;

import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.util.Date;

/**
 * Created by Sergey on 08.07.14.
 */
@Named
@Scope("request")
public class DateManagerBean {

    private Date currentDate = new Date();

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }
}
