package com.bionic.sschudakov.commons.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Base entities type to hold common Id property. To be extended.
 *
 * @author Sergey Chudakov
 * Created by Sergey on 25.06.14.
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = -2435150907688878077L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
