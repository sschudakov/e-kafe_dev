package com.bionic.sschudakov.web;

import com.bionic.sschudakov.entity.OrderItem;
import com.bionic.sschudakov.service.OrderItemService;
import org.springframework.context.annotation.Scope;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;

/**
 * Created by Sergey on 09.07.14.
 */
@Named
@Scope("request")
public class OrderItemBean {

    @Inject
    OrderItemService orderItemService;

    @Inject
    CartBean cartBean;

    public List<OrderItem> getOrderItems() {
        return orderItemService.getAllOrderItems();
    }

    public String getQuery() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("query");
    }

    public void checkIfQueryExists() throws IOException {
        if (orderItemService.getOrderItemById(Long.parseLong(getQuery())) == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("error.xhtml");
        }
    }

    public OrderItem getOrderItemById() {
         return orderItemService.getOrderItemById(Long.parseLong(getQuery()));
    }

//    public void addToCart() {
//        String query = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("query");
//        cartBean.add(orderItemService.getOrderItemById(Long.parseLong(query)));
//    }

}
