package com.bionic.sschudakov.dao;

import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.FoodCategory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class DishDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void saveDish(Dish dish) {
        entityManager.persist(dish);
    }

    public Dish findById(Long id) {
        return entityManager.find(Dish.class, id);
    }
    public Dish findByName(String name) {
        return (Dish) entityManager.createQuery("SELECT d from Dish d where d.name = :name")
                .setParameter("name", name)
                .getSingleResult();
    }

    public List<Dish> findByCategory(FoodCategory foodCategory) {
        return entityManager.createQuery("SELECT d from Dish d where d.foodCategory = :foodCategory")
                .setParameter("foodCategory", foodCategory)
                .getResultList();
    }

    public List<Dish> findAll() {
        return entityManager.createQuery("FROM Dish")
                .getResultList();
    }

    public List<Dish> findBestChoises() {
        return entityManager.createQuery("FROM Dish")
                .setMaxResults(3)
                .getResultList();
    }

    public boolean remove(Long dishId) {
        entityManager.remove(entityManager.getReference(Dish.class, dishId));
        return true;
    }

    public Dish updateDish(Dish dish) {
        entityManager.merge(dish);
        return dish;
    }

    public List<Dish> findAllOrderedByFoodCategoryIndex() {
        return entityManager.createQuery("SELECT d FROM Dish d ORDER BY d.foodCategory.orderIndex")
                .getResultList();
    }
}
