package com.bionic.sschudakov.web;

import com.bionic.sschudakov.dao.DishPriceDao;
import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import com.bionic.sschudakov.entity.OrderItem;
import com.bionic.sschudakov.entity.RestaurantOrder;
import com.bionic.sschudakov.service.OrderItemService;
import com.bionic.sschudakov.service.RestaurantOrderService;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Sergey on 09.07.14.
 */
@Named
@Scope("request")
public class RestaurantOrderBean {

    @Inject
    private RestaurantOrderService restaurantOrderService;

    @Inject
    private OrderItemService orderItemService;

    @Inject
    private CartBean cartBean;

    private String orderNumber;

    private Date orderSubmitDate;

    public String getOrderNumber() {
        return orderNumber;
    }
    public Date getOrderSubmitDate() {
        return orderSubmitDate;
    }

    public String submitOrder(List<CartItem> cartItems, Date orderOpenDate, String clientsPhoneNumber) {

        if (clientsPhoneNumber == null || clientsPhoneNumber.equals("")) {
            return "no_phone_number";
        }

        if (cartItems.isEmpty()) {
            return "empty_cart";
        }

        orderSubmitDate = new Date();

        RestaurantOrder restaurantOrder = new RestaurantOrder();
        restaurantOrder.setDateTimeOpen(new Timestamp(orderOpenDate.getTime()));
        restaurantOrder.setDateTimeSubmit(new Timestamp(orderSubmitDate.getTime()));
        restaurantOrder.setComments(clientsPhoneNumber);

        String response = restaurantOrderService.createRestaurantOrder(restaurantOrder, cartItems);

        if (response.equals("error")) {
            return response;
        }

        // form order number.
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(orderSubmitDate.getTime());
        String yearNumber = Integer.toString(c.get(Calendar.YEAR)).substring(2);
        String orderId = restaurantOrder.getId().toString();
        while (orderId.length() < 4) {
            orderId = "0" + orderId;
        }

        orderNumber = yearNumber + orderId;

        cartBean.cleanCart();

        return response;

    }

    public Date getDeliveryDate() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(orderSubmitDate.getTime());
        c.add(Calendar.HOUR_OF_DAY, 2);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }

}
