package com.bionic.sschudakov.service;

import com.bionic.sschudakov.dao.OrderItemDao;
import com.bionic.sschudakov.dao.RestaurantOrderDao;
import com.bionic.sschudakov.entity.OrderItem;
import com.bionic.sschudakov.entity.RestaurantOrder;
import com.bionic.sschudakov.web.CartItem;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Named
@Transactional
public class RestaurantOrderService {

    @Inject
    RestaurantOrderDao restaurantOrderDao;

    @Inject
    OrderItemDao orderItemDao;

    public RestaurantOrder getRestaurantOrder(Long id) {
        return restaurantOrderDao.findById(id);
    }

    public RestaurantOrder createRestaurantOrder(RestaurantOrder restaurantOrder) {
        restaurantOrderDao.saveRestaurantOrder(restaurantOrder);
        return restaurantOrder;
    }

    public String createRestaurantOrder(RestaurantOrder restaurantOrder, List<CartItem> cartItems) {

        try {
            restaurantOrderDao.saveRestaurantOrder(restaurantOrder);

            for(CartItem c : cartItems) {
                OrderItem orderItem = new OrderItem();
                orderItem.setRestaurantOrder(restaurantOrder);
                orderItem.setDish(c.getDish());
                orderItem.setDishPrice(c.getPrice());
                orderItem.setQuantity(c.getQuantity());
                orderItemDao.saveOrderItem(orderItem);
            }
        } catch (PersistenceException e) {
            return "error";
        }

        return "order_submitted";
    }

    public RestaurantOrder updateRestaurantOrder(RestaurantOrder restaurantOrder) {
        restaurantOrderDao.updateRestaurantOrder(restaurantOrder);
        return restaurantOrder;
    }

    public void removeRestaurantOrder(Long id) {
        restaurantOrderDao.delete(id);
    }

}
