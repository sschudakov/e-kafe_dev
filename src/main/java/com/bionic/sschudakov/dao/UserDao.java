package com.bionic.sschudakov.dao;

import com.bionic.sschudakov.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class UserDao {
    @PersistenceContext
    private EntityManager entityManager;

    public User saveUser(User user) {
        entityManager.persist(user);
        return user;
    }

    public User updateUser(User user) {
        entityManager.merge(user);
        return user;
    }

    public User findById(Long id) {
        return entityManager.find(User.class, id);
    }

    public User findByUserName(String userName) {
        return (User) entityManager.createQuery("SELECT u from User u where u.userName = :userName")
                .setParameter("userName", userName)
                .setMaxResults(1)
                .getSingleResult();
    }


    public List<User> findAll() {
        return entityManager.createQuery("FROM User")
                .getResultList();
    }

    public boolean delete(User user) {
        entityManager.remove(entityManager.getReference(User.class, user.getId()));
        return true;
    }

}
