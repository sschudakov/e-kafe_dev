package com.bionic.sschudakov.web;

import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import com.bionic.sschudakov.entity.FoodCategory;
import com.bionic.sschudakov.service.DishPriceService;
import com.bionic.sschudakov.service.DishService;
import com.bionic.sschudakov.service.FoodCategoryService;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.component.html.HtmlInputHidden;
import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Named
@Scope("session")
public class DishBean {

    @Inject
    private DishService dishService;

    @Inject
    private DishPriceService dishPriceService;

    @Inject
    FoodCategoryService foodCategoryService;

    @Inject
    CartBean cartBean;

    /**
     * Fields to create new dish entity.
     */
    private FoodCategory foodCategory;
    private String name;
    private String description;
    private boolean kitchenMade;


    private List<Dish> dishesByFoodCategory;
    private Dish currentDish;
    private FoodCategory currentFoodCategory;

    private List<Dish> allDishes;
    private boolean updateNeeded = true;

    /**
     * Fields needed to edit single dish information.
     */
    private HtmlDataTable dataTable;
    private Dish dataItem = new Dish();
    private HtmlInputHidden dataItemId = new HtmlInputHidden();

    // Actions -----------------------------------------------------------------------------------
    public String editDataItem() {
        // Get selected Dish item to be edited.
        dataItem = (Dish) dataTable.getRowData();

        // Store the ID of the data item in hidden input element.
        dataItemId.setValue(dataItem.getId());

        return "set_new_dish_price"; //Navigation
    }

    public String saveDataItem() {

        // Retain the ID of the data item from hidden input element.
        dataItem.setId(Long.valueOf(dataItemId.getValue().toString()));

        // Save Dish.
        dishService.update(dataItem);

        return "set_dish_prices";
    }

    // Getters -----------------------------------------------------------------------------------

    public HtmlDataTable getDataTable() {
        return dataTable;
    }

    public Dish getDataItem() {
        return dataItem;
    }

    public HtmlInputHidden getDataItemId() {
        return dataItemId;
    }

    // Setters -----------------------------------------------------------------------------------

    public void setDataTable(HtmlDataTable dataTable) {
        this.dataTable = dataTable;
    }

    public void setDataItem(Dish dataItem) {
        this.dataItem = dataItem;
    }

    public void setDataItemId(HtmlInputHidden dataItemId) {
        this.dataItemId = dataItemId;
    }





    @PostConstruct
    private void init() {
        currentFoodCategory = foodCategoryService.findById(1L);
        dishesByFoodCategory = dishService.findByCategoryId(1L);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FoodCategory getFoodCategory() {
        return foodCategory;
    }

    public void setFoodCategory(FoodCategory foodCategory) {
        this.foodCategory = foodCategory;
    }

    public boolean isKitchenMade() {
        return kitchenMade;
    }

    public void setKitchenMade(boolean kitchenMade) {
        this.kitchenMade = kitchenMade;
    }


    public List<Dish> getAllDishes() {
        if (updateNeeded) {
            allDishes = dishService.getAll();
            updateNeeded = false;
        }
        return allDishes;
    }

    public void setAllDishes(List<Dish> allDishes) {
        this.allDishes = allDishes;
    }

    public Dish getCurrentDish() {
        return currentDish;
    }

    public FoodCategory getCurrentFoodCategory() {
        return currentFoodCategory;
    }

    public List<Dish> getDishesByFoodCategory() {
        return dishesByFoodCategory;
    }

    public String getDishDetails(Long dishId) {
        this.currentDish = dishService.getById(dishId);
        return "dish_item";
    }

    public BigDecimal getCurrentDishPrice() {
        DishPrice dishPrice = dishPriceService.getLastActualDishPrice(currentDish, new Date());
        if (dishPrice != null) {
            Integer price = dishPrice.getPrice();
            return new BigDecimal(price).divide(new BigDecimal(100));
        }
        return BigDecimal.ZERO;
    }

    public void changeFoodCategory(Long currentFoodCategoryId) {
        currentFoodCategory = foodCategoryService.findById(currentFoodCategoryId);
        dishesByFoodCategory = dishService.findByCategoryId(currentFoodCategoryId);
    }

    public void addToCart(Long dishId) {
        cartBean.add(dishService.getById(dishId));
    }

    public BigDecimal getDishPrice(Long dishId) {
        Dish dish = dishService.getById(dishId);
        DishPrice dishPrice = dishPriceService.getLastActualDishPrice(dish, new Date());
        if (dishPrice != null) {
            Integer price = dishPrice.getPrice();
            return new BigDecimal(price).divide(new BigDecimal(100));
        }
        return BigDecimal.ZERO;
    }

    public String loadPageByFoodCategoryId(Long foodCategoryId) {
        currentFoodCategory = foodCategoryService.findById(foodCategoryId);
        dishesByFoodCategory = dishService.findByCategoryId(foodCategoryId);
        return "menu";
    }

    public List<Dish> getAll() {
        return dishService.getAll();
    }

    public String editAction(Dish dish) {
        dish.setEditable(true);
        return null;
    }

    public String delete(Long dishId) {
        dishService.delete(dishId);
        updateNeeded = true;
        return "";
    }

    public String createDish() {
        dishService.createDish(null, name, description, kitchenMade);
        name = null;
        description = null;
        kitchenMade = false;
        updateNeeded = true;
        return "";
    }

    public String saveAction() {
        for (Dish d : allDishes) {
            if (d.isEditable()) {
                dishService.update(d);
            }
        }
        updateNeeded = true;
        return null;
    }
}
