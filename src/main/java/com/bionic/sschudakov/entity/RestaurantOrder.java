package com.bionic.sschudakov.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "restaurant_order")
public class RestaurantOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_time_open")
    private Timestamp dateTimeOpen;

    @Column(name = "date_time_submit")
    private Timestamp dateTimeSubmit;

    @Column(name = "date_time_close")
    private Timestamp dateTimeClose;

    @Column(name = "comments")
    private String comments;

    @Column(name = "deleted")
    private boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getDateTimeOpen() {
        return dateTimeOpen;
    }

    public void setDateTimeOpen(Timestamp dateTimeOpen) {
        this.dateTimeOpen = dateTimeOpen;
    }

    public Timestamp getDateTimeSubmit() {
        return dateTimeSubmit;
    }

    public void setDateTimeSubmit(Timestamp dateTimeSubmit) {
        this.dateTimeSubmit = dateTimeSubmit;
    }

    public Timestamp getDateTimeClose() {
        return dateTimeClose;
    }

    public void setDateTimeClose(Timestamp dateTimeClose) {
        this.dateTimeClose = dateTimeClose;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
