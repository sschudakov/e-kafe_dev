package com.bionic.sschudakov.dao;

import com.bionic.sschudakov.entity.FoodCategory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class FoodCategoryDao {
    @PersistenceContext
    private EntityManager entityManager;

    public FoodCategory saveFoodCategory(FoodCategory foodCategory) {
        entityManager.persist(foodCategory);
        return foodCategory;
    }

    public FoodCategory updateFoodCategory(FoodCategory foodCategory) {
        entityManager.merge(foodCategory);
        return foodCategory;
    }

    public FoodCategory findById(Long id) {
        return entityManager.find(FoodCategory.class, id);
    }

    public FoodCategory findByName(String name) {
        return (FoodCategory) entityManager.createQuery("SELECT f from FoodCategory f where f.name = :name")
                .setParameter("name", name)
                .getSingleResult();
    }


    public List<FoodCategory> findAll() {
        return entityManager.createQuery("FROM FoodCategory")
                .getResultList();
    }

    public List<FoodCategory> findAllOrdered() {
        return entityManager.createQuery("SELECT f FROM FoodCategory f ORDER BY f.orderIndex")
                .getResultList();
    }

    public boolean delete(FoodCategory foodCategory) {
        entityManager.remove(entityManager.getReference(FoodCategory.class, foodCategory.getId()));
        return true;
    }

}
