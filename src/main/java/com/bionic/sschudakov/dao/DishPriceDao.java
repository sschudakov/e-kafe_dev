package com.bionic.sschudakov.dao;

import com.bionic.sschudakov.entity.Dish;
import com.bionic.sschudakov.entity.DishPrice;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Repository
public class DishPriceDao {

    @PersistenceContext
    private EntityManager entityManager;

    public DishPrice saveDishPrice(DishPrice dishPrice) {
        entityManager.persist(dishPrice);
        return dishPrice;
    }

    public DishPrice findById(Long id) {
        return entityManager.find(DishPrice.class, id);
    }

    public List<DishPrice> findAll() {
        return entityManager.createQuery("FROM DishPrice")
                .getResultList();
    }

    /**
     * Method returns actual DishPrice entity set for dish by passed date parameter.
     *
     * @param dish
     * @param date
     * @return
     */
    public DishPrice findLastActualDishPrice(Dish dish, Date date) {
        Timestamp dateTime = new Timestamp(date.getTime());
        DishPrice dishPrice = null;
        List<DishPrice> dishPriceList = (List<DishPrice>) entityManager.createQuery("SELECT d FROM DishPrice d WHERE d.dish=:dish AND d.dateTime < :dateTime ORDER BY d.dateTime DESC")
                .setParameter("dish", dish)
                .setParameter("dateTime", dateTime)
                .setMaxResults(1)
                .getResultList();
        if (!dishPriceList.isEmpty()) {
            dishPrice = dishPriceList.get(0);
        }
        return dishPrice;
    }

    /**
     * Data in dish_price should never benn changed or removed.
     * So update and remove operations are not present in this DAO.
     *
     */

}
