package com.bionic.sschudakov.commons.dao;

import com.bionic.sschudakov.commons.domain.BaseEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Sergey on 01.07.14.
 */
public interface IGenericDao<T extends BaseEntity> {
    @Transactional(readOnly = true)
    T findById(Long id);

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    List<T> findAll();

    T save(T entity);

    T update(T entity);

    void delete(T entity);

    void delete(Long id);

    void flush();
}
