package com.bionic.sschudakov.service;

import com.bionic.sschudakov.dao.UserDao;
import com.bionic.sschudakov.entity.User;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


@Named
@Transactional
public class UserService {

    @Inject
    private UserDao userDao;


    public void createUser(User user) {
        userDao.saveUser(user);
    }

    public void updateFoodCategory(User user) {
        userDao.updateUser(user);
    }

    public User findById(Long id) {
        return userDao.findById(id);
    }

    public User findUser(String userName) {
        return userDao.findByUserName(userName);
    }

    public List<User> getAll() {
        return userDao.findAll();
    }

    public boolean delete(User user) {
        return userDao.delete(user);
    }

}
