package com.bionic.sschudakov.entity;

import javax.persistence.*;

@Entity
@Table(name = "dish")
public class Dish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="food_category_id")
    private FoodCategory foodCategory;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="kitchen_made")
    private boolean kitchenMade;

    @Column(name="img_path")
    private String imgPath;

    @Transient
    private boolean editable = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FoodCategory getFoodCategory() {
        return foodCategory;
    }

    public void setFoodCategory(FoodCategory foodCategory) {
        this.foodCategory = foodCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isKitchenMade() {
        return kitchenMade;
    }

    public void setKitchenMade(boolean kitchenMade) {
        this.kitchenMade = kitchenMade;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
