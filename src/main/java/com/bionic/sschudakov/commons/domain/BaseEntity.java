package com.bionic.sschudakov.commons.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Base entity type to hold common Id property. To be extended.
 *
 * @author Sergey Chudakov
 * Created by Sergey on 25.06.14.
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = -2435150907688878077L;

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
